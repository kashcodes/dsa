package com.codes.datastructures;

class Array_Imp {
	

	String[] str = new String[10];
	
	public void root(String key) {
		str[0] = key;
	}
 	
	public void set_left(String key, int root) {
		int t = root*2+1;
		if(str[root] == null) {
			System.out.println("root element not found for left child");
		} else {
			str[t] = key;
		}
	}
	
	public void set_right(String key, int root) {
		int t = root*2+2;
		if(str[root] == null) {
			System.out.println("root element not found for right child");
		} else {
			str[t] = key;
		}
	}

	public void print() {
		for(int i = 0; i < str.length; i++ ) {
			if(str[i] != null) {
				System.out.print(str[i]);
			} else {
				System.out.print("-");
			}
		}
	}
	
}

public class BinaryTreeArray {
	
	public static void main(String[] args) {
		Array_Imp tree = new Array_Imp();
		tree.root("A");
		tree.set_left("B", 0);
		tree.set_right("C", 0);
		tree.set_left("D", 1);
		tree.set_right("E", 1);
		tree.set_right("F", 2);
		tree.print();
	}
	
	

}
