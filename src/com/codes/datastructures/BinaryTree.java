package com.codes.datastructures;

import java.util.LinkedList;
import java.util.Queue;

class Node {
	int data;
	Node left;
	Node right;

	public Node(int item) {
		this.data = item;
		left = right = null;
	}
}

public class BinaryTree {

	// Root of the Binary Tree
	Node root;

	public BinaryTree() {
		root = null;
	}
	
	//Level order is BFS
	//Step1 : Find height of the tree
	//Step2 : print height wise
	private void printLevelOrder() {

		int h = height(root);
		int i = 0;
		for(i = 1; i <= h; i++) {
			printCurrentLevel(root,i);
		}

	}

	//height using recursion
	private int height(Node root) {
		
		if(root == null) {
			return 0;
		} else {
			int lheight = height(root.left);
			int rheight = height(root.right);
			
			if(lheight > rheight) {
				return lheight + 1;
			} else {
				return rheight + 1;
			}
		}
	}
	
	private void printCurrentLevel(Node root, int level) {
		
		if(root == null) {
			return;
		} 
		
		if(level == 1) {
			System.out.print(root.data + " ");
		} else if(level > 1) {
			printCurrentLevel(root.left, level - 1);
			printCurrentLevel(root.right, level - 1);
		}
		
	}
	
	
	private void printLevelOrderQueue() {
		System.out.println();
		Queue<Node> tree_queue = new LinkedList<>();
		tree_queue.add(root);
		
		while(!tree_queue.isEmpty()) {
			Node current = tree_queue.poll();
			System.out.print(current.data + " ");
			if(current.left != null) {
				tree_queue.add(current.left);
			}
			if(current.right != null) {
				tree_queue.add(current.right);
			}
			
			
		}
		
		
	}

	public static int heightUsingQueue(Node root) {
		Queue<Node> tree_queue = new LinkedList<>();
		
		int height = 0;
		tree_queue.add(root);
		tree_queue.add(null);
		
		while(!tree_queue.isEmpty()) {
			Node temp = tree_queue.poll();
			
			if(temp == null) {
				height++;
			}
			
			if(temp != null) {
				if(temp.left != null) {
					tree_queue.add(temp.left);
				}
				if(temp.right != null) {
					tree_queue.add(temp.right);
				}
			} else if(!tree_queue.isEmpty()) {
				tree_queue.add(null);
			}
			
			
		}
		
		return height;
	}
	
	public static int heightUsingQueueWithoutNull(Node root)
    {
 
        // Initialising a variable to count the
        // height of tree
        Queue<Node> q = new LinkedList<Node>();
        q.add(root);
        int height = 0;
        while (!q.isEmpty()) {
            int size = q.size();
            for (int i = 0; i < size; i++) {
                Node temp = q.poll();
                if (temp.left != null) {
                    q.add(temp.left);
                }
                if (temp.right != null) {
                    q.add(temp.right);
                }
            }
            height++;
        }
        return height;
    }
	
	private static void levelOrderInsertion(Node temp, int key) {
		Queue<Node> tree_queue = new LinkedList<>();
		if(temp == null) {
			temp = new Node(key);
			return;
		}
		tree_queue.add(temp);
		while(!tree_queue.isEmpty()) {
			temp = tree_queue.poll();
			if(temp.left == null) {
				temp.left = new Node(key);
				break;
			} else {
				tree_queue.add(temp.left);
			}
			
			if(temp.right == null) {
				temp.right = new Node(key);
				break;
			} else {
				tree_queue.add(temp.right);
			}
		}
	}
	
	private void inorder(Node temp) {
		if(temp == null) return;
		
		inorder(temp.left);
		System.out.print(temp.data + " ");
		inorder(temp.right);
	}
	
	private Node delete(Node root, int key) {
		
		if(root == null) return null;
		if(root.left == null && root.right == null) {
			if(root.data != key) {
				return root;
			} else return null;
		}
		Queue<Node> tree_queue = new LinkedList<>();
		Node temp = null, keyNode = null, last = null;
		tree_queue.add(root);
		
		while(!tree_queue.isEmpty()) {
			temp = tree_queue.poll();
			
			if(temp.data == key) {
				keyNode = temp;
			}
			if(temp.left != null) {
				last = temp;
				tree_queue.add(temp.left);
			}
			if(temp.right != null) {
				last = temp;
				tree_queue.add(temp.right);
			}
		}
		
		if(keyNode != null) {
			keyNode.data = temp.data;
			if(last.right == temp) {
				last.right = null;
			} else {
				last.left = null;
			}
		}
		
		return root;
		
		
	}
    
	
	public static void main(String[] args) {

		BinaryTree tree = new BinaryTree();
		tree.root = new Node(1);

		tree.root.left = new Node(2);
		tree.root.right = new Node(3);

		tree.root.left.left = new Node(4);
		tree.root.left.right = new Node(5);

		tree.root.right.left = new Node(6);
		tree.root.right.right = new Node(7);
		
		tree.root.left.left.left = new Node(8);
		
		levelOrderInsertion(tree.root, 12);
		
		int h2 = heightUsingQueueWithoutNull(tree.root);
		int h = heightUsingQueue(tree.root);
		System.out.println(h2);
		tree.printLevelOrder();
		tree.printLevelOrderQueue();
		System.out.println();
		System.out.println("inoder traversal");
		tree.inorder(tree.root);
		
		tree.printLevelOrder();
		tree.root = tree.delete(tree.root, 4);
		tree.printLevelOrder();

	}

}
