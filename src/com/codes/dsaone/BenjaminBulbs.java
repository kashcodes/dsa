package com.codes.dsaone;

import java.util.Scanner;

//
//There are n bulbs that are initially off. 
//You first turn on all the bulbs, then you turn off every second bulb.
//
//On the third round, you toggle every third bulb (turning on if it's off or turning off if it's on). For the ith round, you toggle every i bulb. For the nth round, you only toggle the last bulb.
//
//Return the number of bulbs that are on after n rounds.

//
//Solution:
//	Only the numbers which are perfect squares will be on after all the cycle
public class BenjaminBulbs {
	
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		
		for(int i = 1; i*i <= n; i++) {
			System.out.println(i*i);
		}
	}

}
