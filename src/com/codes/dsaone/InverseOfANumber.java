package com.codes.dsaone;

import java.util.Scanner;

public class InverseOfANumber {
	
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		
		int inv = 0;
		int originalPosition = 1;
		while(n != 0) {
			int originalDigit = n%10;
			int inverseDigit = originalPosition;
			int invertedPosition = originalDigit;
			
			inv = inv + inverseDigit * (int)Math.pow(10, invertedPosition -1);
			
			n = n/10;
			originalPosition++;
		}
		
		System.out.println(inv);
	}

}
