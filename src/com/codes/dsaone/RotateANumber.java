package com.codes.dsaone;

import java.util.Scanner;

public class RotateANumber {
	
	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int k = in.nextInt();
		
		int temp = n;
		int nod = 0;
		while(temp > 0) {
			temp = temp / 10;
			nod++;
		}
		
		//when k > nod, ex k = 352
		k = k%nod;
		//when k is negative
		if(k < 0) {
			k = k + nod;
		}
		
		int div = 1;
		int mul = 1; 
		for(int i = 1; i <= nod; i++) {
			if(i <= k) {
				div = div*10;
			} else {
				mul = mul*10;
			}
		}
		
		int q = n/div;
		int r = n%div;
		
		int res = r * mul + q;
		System.out.println(res);
	}

}
