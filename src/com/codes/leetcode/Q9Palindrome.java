package com.codes.leetcode;

import java.util.Scanner;

public class Q9Palindrome {
	
	public static boolean isPalindromeSol2(int x) {

        if(x < 0 ) {
            return false;
        }
        int tempX = x;
        int reverse = 0;

        while(x > 0) {
           reverse = reverse*10 + x%10;
           x = x/10; 
        }


        if(tempX == reverse) {
            return true;
        } else {
            return false;
        }

    }
	
    public static boolean isPalindrome(int x) {

        if(x < 0 ) {
            return false;
        }
        int tempX = x;
        int reverse = 0;

        while(x > 0) {
           reverse = reverse*10 + x%10;
           x = x/10; 
        }


        if(tempX == reverse) {
            return true;
        } else {
            return false;
        }




    }
	
	public static void main(String[] args) {
		Scanner scn = new Scanner(System.in);
		int x = scn.nextInt();
		System.out.println(isPalindrome(x));
		
	}
	

}
