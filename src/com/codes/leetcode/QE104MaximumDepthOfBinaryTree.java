package com.codes.leetcode;

class Node {
	int data;
	Node left, right;
	
	public Node(int item) {
		data = item;
		left = right = null;
	}
}


public class QE104MaximumDepthOfBinaryTree {
	
	Node root;
	
	public static int maxDepth(Node root) {
		
		if(root == null) {
			return 0;
		}
		
		int lheight = maxDepth(root.left);
		int rheight = maxDepth(root.right);
		return Math.max(lheight, rheight) + 1;
		
	}
	
	public static void main(String[] args) {
		QE104MaximumDepthOfBinaryTree tree = new QE104MaximumDepthOfBinaryTree();
		tree.root = new Node(1);
		tree.root.left = new Node(2);
		tree.root.right = new Node(3);
		tree.root.right.right = new Node(7);
		int height = maxDepth(tree.root);
		System.out.println(height);
	}
	
	
	
	

}
