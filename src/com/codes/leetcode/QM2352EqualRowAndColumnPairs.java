package com.codes.leetcode;

public class QM2352EqualRowAndColumnPairs {

	private static int equalPairs(int[][] grid) {
		
		int count = 0;
		
		for(int i = 0; i < grid.length; i++) {
			for(int j = 0; j < grid.length; j++) {
				if(checkIfEqual(i,j,grid)) {
					count++;
				}
			}
		}
		
		return count;
	}
	
	private static boolean checkIfEqual(int i, int j, int[][] grid) {
		for(int k = 0; k < grid.length; k++) {
			if(grid[i][k] != grid[k][j]) {
				return false;
			}
		}
		return true;
	}
	
	public static void main(String[] args) {
		//int[][] grid = { { 3, 2, 1 }, { 1, 7, 6 }, { 2, 7, 7 } };
		int[][] grid = { { 3,1,2,2 }, { 1,4,4,5 }, { 2,4,2,2}, {2,4,2,2} };
		System.out.println(equalPairs(grid));
	}

}
