package com.codes.leetcode;

import java.util.ArrayList;

public class Demo {
	
	public static void main(String[] args) {
		
		ArrayList<int[]> list = new ArrayList<>();
		list.add(new int[] {1,3});
		int[] fetch = list.get(list.size()-1);
		fetch[1] = 4;
		for(int[] one : list) {
			System.out.println(one[1]);
		}
		
		int[] bakaiti = new int[] {1,3};
		int[] nayaBakaiti = bakaiti;
		nayaBakaiti[1] = 4;
		System.out.println(bakaiti[1]);
		
		ArrayList<Integer> list1 = new ArrayList<>();
		list1.add(1);
		int i = list1.get(list1.size()-1);
		i = 2;
		for(int one : list1) {
			System.out.println(one);
		}
		
	}

}
