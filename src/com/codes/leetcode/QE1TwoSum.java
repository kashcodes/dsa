package com.codes.leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QE1TwoSum {
	
	public static int[] twoSumBruteForce(int[] nums, int target) {
		
		for(int i = 0 ; i < nums.length; i++) {
			for(int j = 0; j < nums.length; j++) {
				if(nums[i] + nums[j] == target) {
					return new int[] {i,j};
				}
			}
		}
		return new int[] {};
	}
	
	public static int[] twoSumHashMap(int[] nums, int target) {
		Map<Integer, Integer> map = new HashMap<>();
		for(int i = 0; i < nums.length; i++) {
			int complement = target - nums[i];
			if(map.containsKey(complement)) {
				return new int[] {map.get(complement), i};
			}
			map.put(nums[i], i);
		}
		return new int[] {};
	}
	
	public static void main(String[] args) {
		int[] nums = new int[] {1,2,3,4,5};
		int[] response = twoSumHashMap(nums,9);
		System.out.println(response[0] + " " + response[1]);
	}

}
