package com.codes.leetcode;

import java.util.ArrayList;
import java.util.Arrays;

public class QM56MergeIntervals {
	
	private static int[][] mergeIntervals(int[][] intervals) {
		
		Arrays.sort(intervals, (a,b) -> Integer.compare(a[0],b[0]));
		
		ArrayList<int[]> list = new ArrayList<>();
		
		for(int[] interval : intervals) {
			if(list.size() == 0) {
				list.add(interval);
			} else {
				int[] prevInterval = list.get(list.size()-1);
				if(interval[0] <= prevInterval[1]) {
					prevInterval[1] = Math.max(interval[1], prevInterval[1]);
				} else {
					list.add(interval);
				}
			}
		}
		return list.toArray(new int[list.size()][]);
	}
	
	public static void main(String[] args) {
		int[][] intervals = new int[][] {{1,3},{2,6},{8,10},{15,18}};
		int[][] mergedIntervals = mergeIntervals(intervals);
		for(int[] interval : mergedIntervals) {
			System.out.println(interval[0] + " " + interval[1]);
		}
	}

}
