package com.codes.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class QM15ThreeSum {
	
	private static List<List<Integer>> threeSum(int[] nums){
		Set<List<Integer>> response = new HashSet<>();
		Arrays.sort(nums);
		for(int i = 0; i < nums.length; i++) {
			HashSet<Integer> s = new HashSet<>();
			int currSum = 0-nums[i];
			for(int j = i+1; j < nums.length; j++) {
				if(s.contains(currSum - nums[j])) {
					List<Integer> numsList = new ArrayList<>();
					numsList.add(nums[i]);
					numsList.add(nums[j]);
					numsList.add(currSum-nums[j]);
					response.add(numsList);
				}
				s.add(nums[j]);
			}
			
		}
		return new ArrayList<>(response);
	}
	
	public static void main(String[] args) {
		int[] nums = new int[] {-1,0,1,2,-1,-4};
		List<List<Integer>> response = threeSum(nums);
		System.out.println(response);
	}

}
