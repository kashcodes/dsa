package com.codes.leetcode;

import java.util.ArrayList;
import java.util.List;

public class QE228SummaryRanges {
	
	public static List<String> summaryRanges(int[] nums) {
		
		
        List<String> list = new ArrayList<>();
        
        if (nums == null || nums.length == 0) {
            return list;
        }
        
        int start = nums[0];
        
        for(int i = 1; i < nums.length; i++) {
        	if(nums[i] != nums[i-1]+1) {
        		if(start == nums[i-1]) {
        			list.add(Integer.toString(nums[i-1]));
        		} else {
            		list.add(Integer.toString(start)+"->"+(Integer.toString(nums[i-1])));
        		}
        		start = nums[i];
        	}
        }
        
        if (start == nums[nums.length - 1]) {
            list.add(Integer.toString(start));
        } else {
            list.add(start + "->" + nums[nums.length - 1]);
        }
        
        
        return list;
    }
	
	public static void main(String[] args) {
		int[] nums = {0,1,2,3,5,6};
		System.out.println(summaryRanges(nums).toString());
	}

}
