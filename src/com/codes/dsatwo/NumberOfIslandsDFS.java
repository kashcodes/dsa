package com.codes.dsatwo;

import java.util.Scanner;

public class NumberOfIslandsDFS {
	
	private static void markIsland(int[][] grid, int i, int j, int rows, int cols) {
		
		if(i < 0 || i >= rows || j < 0 || j >= cols || grid[i][j] != 1) {
			return;
		}
		
		grid[i][j] = 2;
		markIsland(grid, i+1, j, rows, cols);
		markIsland(grid, i, j+1, rows, cols);
		markIsland(grid, i-1, j, rows, cols);
		markIsland(grid, i, j-1, rows, cols);
		
	}

	public static void main(String[] args) {

		// Input rows, cols and grid
		Scanner scn = new Scanner(System.in);
		int rows = scn.nextInt();
		int cols = scn.nextInt();

		int[][] grid = new int[rows][cols];

		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				grid[i][j] = scn.nextInt();
			}
		}

		int islands = 0;
		for (int i = 0; i < rows; ++i) {
			for (int j = 0; j < cols; ++j) {
				if(grid[i][j] == 1) {
					markIsland(grid,i,j,rows,cols);
					islands++;
				}
			}
		}
		
		System.out.println(islands);

	}

}
