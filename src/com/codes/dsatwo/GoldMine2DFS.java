package com.codes.dsatwo;

import java.util.ArrayList;
import java.util.Scanner;

/*
Max connected component sum
1. You are given a number n, representing the number of rows.
2. You are given a number m, representing the number of columns.
3. You are given n*m numbers, representing elements of 2d array a, which represents a gold mine.
4. You are allowed to take one step left, right, up or down from your current position.
5. You can't visit a cell with 0 gold. 
6. Each cell has a value that is the amount of gold available in the cell.

You are required to identify the maximum amount of gold that can be dug out from the mine if you start and stop collecting gold from any position in the grid that has some gold.
*/

public class GoldMine2DFS {
	static int max = 0;

	private static void travelAndCollectGold(int[][] grid, int i, int j, boolean[][] visited, ArrayList<Integer> bag) {

		//if while traversing, we go out of the grid or if there is no gold in a cell or if the cell is already visited then return
		if (i < 0 || i >= grid.length || j < 0 || j >= grid[0].length || grid[i][j] == 0 || visited[i][j] == true) {
			return;
		}

		// as soon as we enter the correct cell, mark it as visited and store the value in bag
		visited[i][j] = true;
		bag.add(grid[i][j]);

		//travel towards all four directions, north, south, east, west
		travelAndCollectGold(grid, i + 1, j, visited, bag);
		travelAndCollectGold(grid, i, j + 1, visited, bag);
		travelAndCollectGold(grid, i - 1, j, visited, bag);
		travelAndCollectGold(grid, i, j - 1, visited, bag);

	}

	private static void solve(int[][] grid) {
		
		boolean[][] visited = new boolean[grid.length][grid[0].length];
		// loop through each cell
		for (int i = 0; i < grid.length; i++) {
			for (int j = 0; j < grid[0].length; j++) {
				if (grid[i][j] != 0 || visited[i][j] == false) {
					// create a bag
					ArrayList<Integer> bag = new ArrayList<>();
					// start travelling and storing gold in bag
					travelAndCollectGold(grid, i, j, visited, bag);
					int sum = 0;
					// sum up the gold in the bag
					for (int val : bag) {
						sum += val;
					}
					if (sum > max) {
						max = sum;
					}
				}
			}
		}
		System.out.println(max);
	}

	public static void main(String[] args) {

		Scanner scn = new Scanner(System.in);
		int rows = scn.nextInt();
		int cols = scn.nextInt();

		int[][] grid = new int[rows][cols];

		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				grid[i][j] = scn.nextInt();
			}
		}

		solve(grid);
	}

}
