package com.codes.dsatwo;

import java.util.Scanner;

/*
Trace the code where inputs are
4
dog cat dad good
9
a b c d d d g o o
1 0 9 5 0 3 0 0 0 0 0 0 0 2 0 0 0 0 0 0 0 0 0 0 0

This is a recursive Java program that calculates the maximum score of words that can be formed from a given set of letters.

The program starts with a main method that takes input from the user using the Scanner class. It reads the number of words, the words themselves, the number of letters, the letters, and a score for each letter.

The main method then initializes an integer array farr that keeps track of the frequency of each letter in the given letters set. It calls the solution method with the parameters words, farr, score, and idx set to 0.

The solution method is a recursive function that takes four parameters: an array of words, an array of the frequency of letters in the given letters set, an array of scores for each letter, and an index of the current word being processed.

The base case for the recursion is when idx equals the length of the words array. When this happens, the function returns 0.

If the base case is not met, the function first calls itself recursively with the index idx+1 and stores the result in an integer variable sno.

Next, the function initializes an integer variable sword to 0 and a boolean variable flag to true. It loops through the characters of the current word and calculates the sword score by adding the score of each letter. If the frequency of any letter is 0 in the farr array, the flag variable is set to false.

If flag is true, the function calculates the syes score by adding the sword score to the result of a recursive call to the function with the index idx+1.

The function then loops through the characters of the current word again and increments the frequency of each letter in the farr array.

Finally, the function returns the maximum of sno and syes.

As the program makes two recursive calls at each level, the time complexity of this recursive function is O(2^n), where n is the number of words. However, the use of memoization can improve the time complexity to O(n * m), where m is the length of the longest word.
*/

public class MaximumScoreOfWords {
	

	private static int solution(String[] words, int[] farr, int[] score, int idx) {
		
		if(idx == words.length) {
			return 0;
		}
		
		int sno = solution(words, farr, score, idx+1);
		
		int sword = 0;
		String word = words[idx];
		boolean flag = true;
		
		for(int i = 0; i < word.length(); i++) {
			char ch = word.charAt(i);
			if(farr[ch - 'a'] == 0) {
				flag = false;
			}
			farr[ch - 'a']--;
			sword += score[ch - 'a'];
		}
		
		int syes = 0;
		if(flag) {
			syes = sword + solution(words, farr, score, idx + 1);
		}
		for(int i = 0; i < word.length(); i++) {
			char ch = word.charAt(i);
			farr[ch - 'a']++;
		}
		
		return Math.max(sno, syes);
		
	}
	
	public static void main(String[] args) {
		
		Scanner scn = new Scanner(System.in);
		int noOfWords = scn.nextInt();
		String[] words = new String[noOfWords];
		for(int i = 0; i < noOfWords; i++) {
			words[i] = scn.next();
		}
		
		int noOfLetters = scn.nextInt();
		char[] letters = new char[noOfLetters];
		for(int i = 0; i < noOfLetters; i++) {
			letters[i] = scn.next().charAt(0);
		}
		
		int[] score = new int[26];
		for(int i = 0; i < score.length; i++) {
			score[i] = scn.nextInt();
		}
		if(words == null || words.length == 0 || letters == null || letters.length == 0 || score.length == 0) {
			System.out.println(0);
			return;
		}
		int[] farr = new int[score.length];
		for(char ch : letters) {
			farr[ch - 'a']++;
		}
		System.out.println(solution(words, farr, score, 0));
	}


}
