package com.codes.dsatwo;

import java.util.Scanner;

/*
Flood Fill problem where we are required to print the path from the top left corner to the bottom right corner of a 2-D array with avoiding obstacles present in the array. 
In this problem, only one move is allowed at a time and can be moved in the top, left, down or right direction.
*/

public class FloodFill {
	
	private static void floodFill(int[][] grid, int i, int j, String psf, boolean[][] visited) {
		
		if(i < 0 || i >= grid.length || j < 0 || j >= grid[0].length || grid[i][j] == 1 || visited[i][j] == true) {
			return;
		}
		
		if(i == grid.length - 1 && j == grid[0].length - 1) {
			System.out.println(psf);
			return;
		}
		
		visited[i][j] = true;
		
		floodFill(grid, i-1, j, psf + "t", visited);
		floodFill(grid, i, j-1, psf + "l", visited);
		floodFill(grid, i+1, j, psf + "d", visited);
		floodFill(grid, i, j+1, psf + "r", visited);
		
	}
	
	public static void main(String[] args) {
		
		Scanner scn = new Scanner(System.in);
		
		int rows = scn.nextInt();
		int cols = scn.nextInt();
		
		int[][] grid = new int[rows][cols];
		boolean[][] visited = new boolean[rows][cols];
		
		for(int i = 0; i < rows; i++) {
			for(int j = 0; j < cols; j++) {
				grid[i][j] = scn.nextInt();
			}
		}
		
		floodFill(grid, 0, 0, "", visited);
		
	}

	

}
