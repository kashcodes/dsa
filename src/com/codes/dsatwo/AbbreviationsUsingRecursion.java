package com.codes.dsatwo;

import java.util.Scanner;

//Given a string of characters of length less than 10. 
//We need to print all the alpha-numeric abbreviation of the string. 
//The alpha-numeric abbreviation is in the form of characters mixed with the digits which is equal 
//to the number of skipped characters of a selected substring. 
//
//So, whenever a substring of characters is skipped, you have to replace it with the digit 
//denoting the number of characters in the substring. 
//There may be any number of skipped substrings of a string. 
//No two substrings should be adjacent to each other. 
//Hence, no two digits are adjacent in the result.

public class AbbreviationsUsingRecursion {
	
	private static void solution(String str, String asf, int count, int pos) {
		// TODO Auto-generated method stub
		
		if(pos == str.length()) {
			if(count == 0) {
				System.out.println(asf);
			} else {
				System.out.println(asf + count);
			}
			
			return;
		}
		
		if(count > 0) {
			solution(str, asf + count + str.charAt(pos), 0, pos + 1);
		} else {
			solution(str, asf + str.charAt(pos), 0, pos + 1);
		}
		
		solution(str, asf, count + 1, pos + 1);
		
	}
	
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String str = in.nextLine();
		solution(str,"",0,0);
		
	}

}
