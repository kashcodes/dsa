package com.codes.dsatwo;

import java.util.Arrays;
import java.util.Scanner;

public class MaximumScoreOfWordsGPT {
	private static int solution(String[] words, int[] farr, int[] score, int idx, int[][] cache) {
	    
	    if(idx == words.length) {
	        return 0;
	    }
	    
	    if(cache[idx][Arrays.hashCode(farr)] != 0) {
	        return cache[idx][Arrays.hashCode(farr)];
	    }
	    
	    int sno = solution(words, farr, score, idx+1, cache);
	    
	    int sword = 0;
	    String word = words[idx];
	    boolean flag = true;
	    
	    for(int i = 0; i < word.length(); i++) {
	        char ch = word.charAt(i);
	        if(farr[ch - 'a'] == 0) {
	            flag = false;
	        }
	        farr[ch - 'a']--;
	        sword += score[ch - 'a'];
	    }
	    
	    int syes = 0;
	    if(flag) {
	        syes = sword + solution(words, farr, score, idx + 1, cache);
	    }
	    for(int i = 0; i < word.length(); i++) {
	        char ch = word.charAt(i);
	        farr[ch - 'a']++;
	    }
	    
	    int result = Math.max(sno, syes);
	    cache[idx][Arrays.hashCode(farr)] = result;
	    return result;
	    
	}
	
	public static void main(String[] args) {
		
		Scanner scn = new Scanner(System.in);
		int noOfWords = scn.nextInt();
		String[] words = new String[noOfWords];
		for(int i = 0; i < noOfWords; i++) {
			words[i] = scn.next();
		}
		
		int noOfLetters = scn.nextInt();
		char[] letters = new char[noOfLetters];
		for(int i = 0; i < noOfLetters; i++) {
			letters[i] = scn.next().charAt(0);
		}
		
		int[] score = new int[26];
		for(int i = 0; i < score.length; i++) {
			score[i] = scn.nextInt();
		}
		if(words == null || words.length == 0 || letters == null || letters.length == 0 || score.length == 0) {
			System.out.println(0);
			return;
		}
		int[] farr = new int[score.length];
		for(char ch : letters) {
			farr[ch - 'a']++;
		}
		int[][] cache = new int[words.length][words.length]; // initialize cache array
		System.out.println(solution(words, farr, score, 0, cache)); // call solution function with cache array
	}




}
