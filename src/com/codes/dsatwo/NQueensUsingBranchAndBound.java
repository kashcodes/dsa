package com.codes.dsatwo;

import java.util.Scanner;

public class NQueensUsingBranchAndBound {
	
	public static void solve(boolean board[][], int row, boolean cols[],
			boolean ndiag[],boolean rdiag[], String asf) {
		
		for(int col = 0 ; col < board[0].length; col++) {
			
			if(row == board.length) {
				System.out.println(asf + ".");
				return;
			}
			
			if(cols[col] == false && ndiag[row + col] == false 
					&& rdiag[row - col + board.length - 1] == false) {
				
				cols[col] = true;
				ndiag[row + col] = true;
				rdiag[row - col + board.length -1] = true;
				solve(board, row + 1, cols, ndiag, rdiag, asf + row + "-" + col + ",");
				cols[col] = false;
				ndiag[row + col] = false;
				rdiag[row - col + board.length -1] = false;
			}
		}
		
	}
	
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		boolean board[][] = new boolean[n][n];
		boolean cols[] = new boolean[n];
		boolean ndiag[] = new boolean[2 * n - 1];
		boolean rdiag[] = new boolean[2 * n - 1];
		
		solve(board, 0, cols, ndiag, rdiag, "");
	}

}
