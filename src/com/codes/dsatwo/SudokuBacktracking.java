package com.codes.dsatwo;

import java.util.Scanner;

/*
1. You are give a partially filled 9*9 2-D array(arr) which represents an incomplete Sudoku state.
2. You are required to assign the digits from 1 to 9 to the empty cells following some rules.
Rule 1 - Digits from 1-9 must occur exactly once in each row.
Rule 2 - Digits from 1-9 must occur exactly once in each column.
Rule 3 - Digits from 1-9 must occur exactly once in each 3x3 sub-array of the given 2D array.

Assumption - The given Sudoku puzzle will have a single unique solution.
*/

public class SudokuBacktracking {
	
	private static void display(int[][] board) {
		for(int i = 0; i < board.length; i++) {
			for(int j = 0; j < board[0].length; j++) {
				System.out.print(board[i][j] + " ");
			}
			System.out.println();
		}
	}
	
	private static void solveSudoku(int[][] board, int i, int j) {
		//if we complete the last cell, then we display the completed board and return   
		if(i == board.length) {
			display(board);
			return;
		}
		// ni and nj are next i and next j, for tracing each cell one by one in recursive call, not in loop
		int ni = 0;
		int nj = 0;
		
		//if we reach the end of column, we go to next row with 0th col else we increment col and row is same
		if(j == board[0].length - 1) {
			ni = i + 1;
			nj = 0;
		} else {
			ni = i;
			nj = j + 1;
		}
		
		//if the cell already has a number we move ahead
		//else we try to fill the box with all possible answers(1-9) and check if it is valid or not
		//if valid, we put that entry in cell and move to the next cell.. 
		//if we reach a cell in which nothing can be filled, we do a backtrack and set the previous cells with 0
		if(board[i][j] != 0) {
			solveSudoku(board, ni, nj);
		} else {
			for(int po = 1; po <= 9; po++) {
				if(isValid(board, i, j, po) == true) {
					board[i][j] = po;
					solveSudoku(board, ni, nj);
					board[i][j] = 0;
				}
			}
			
		}
		
	}
	
	private static boolean isValid(int[][] board, int x, int y, int val) {
		//check if the same number exists in that row (for the row we increment cols for obvious reasons)
		for(int j = 0; j < board[0].length; j++) {
			if(board[x][j] == val) {
				return false;
			}
		}
		
		//check if the same number exists in that col (for the row we increment rows for obvious reasons)
		for(int i = 0; i < board.length; i++) {
			if(board[i][y] == val) {
				return false;
			}
		}
		
		//for checking in the smaller matrix
		//if we are at any particular cell, we can calculate the position(smi, smj) of the start cell of the matrix
		//using the formula below
		int smi = x / 3 * 3;
		int smj = y / 3 * 3;
		
		//check if the value exists in its particular small matrix
		for(int i = 0; i < 3; i++) {
			for(int j = 0; j < 3; j++) {
				if(board[smi + i][smj + j] == val) {
					return false;
				}
			}
		}
		
		return true;
	}

	public static void main(String[] args) {
		
		Scanner scn = new Scanner(System.in);
		int[][] board = new int[9][9];
				
		for(int i = 0; i < 9; i++) {
			for(int j = 0; j < 9; j++) {
				board[i][j] = scn.nextInt();
			}
		}
		
		solveSudoku(board, 0, 0);
		
	}

}
